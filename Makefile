.PHONY: run run-watch test test-watch test-mongo

run:
	npm start

run-watch:
	nodemon server.js

test:
	npm test

test-watch:
	nodemon -x "npm test"

test-mongo:
	docker run -p 27017:27017 -d mongo