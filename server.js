const App = require('./src/app')
const apiSettings = require('./settings')

async function server () {
  const app = await new App().setup()

  await app.listen(apiSettings.PORT)
  console.log(`User API running on port ${apiSettings.PORT}`)
}

server()
