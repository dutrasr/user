const apiSettings = {
  PORT: 8080,
  DATABASE_URI: process.env.DATABASE_URI || 'mongodb://localhost:27017',
  DATABASE_NAME: 'user',
  BASE_PATH: '/',
}

if (process.env.NODE_ENV === 'test') {
  apiSettings.DATABASE_NAME += '_test'
}

module.exports = apiSettings
