const express = require('express')
const router = express.Router()

const pkg = require('../../package.json')

router.get('/', (_, res) => {
  return res
    .json({
      message: pkg.description,
    })
})

router.get('/health', (_, res) => {
  return res
    .status(200)
    .json({
      app: pkg.name,
      version: pkg.version,
    })
})

module.exports = router
