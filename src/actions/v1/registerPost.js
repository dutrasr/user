const express = require('express')
const router = express.Router()

const checkFormMiddleware = require('../../middlewares/checkFormMiddleware')
const checkFormsContentMiddleware = require('../../middlewares/checkFormsContentMiddleware')
const checkUserRegisterMiddleware = require('../../middlewares/checkUserRegisterMiddleware')

const UserModel = require('../../models/userModel')
const CurriculumModel = require('../../models/curriculumModel')

router.use('/user/register', checkFormMiddleware)
router.use('/user/register', checkFormsContentMiddleware)
router.use('/user/register', checkUserRegisterMiddleware)

router.post('/user/register', async (req, res) => {
  const { user, curriculum } = req.body

  const User = new UserModel({ ...user })
  const Curriculum = new CurriculumModel({ ...curriculum })

  const session = await UserModel.startSession()
  session.startTransaction()
  try {
    await User.save()
    await Curriculum.save()

    await session.commitTransaction()

    session.endSession()
  } catch (e) {
    await session.abortTransaction()
    session.endSession()

    return res
      .status(500)
  }

  return res
    .status(200)
    .json({
      success: true,
      message: 'User register with success!',
    })
})

module.exports = router
