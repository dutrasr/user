const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const apiSettings = require('../settings')
const routes = require('./actions/routes')

class App {
  constructor () {
    this.app = express()
  }

  async setup () {
    await this.setupMongooseConnection()
    this.setupBodyParser()
    this.setupRouter()

    return this.app
  }

  async setupMongooseConnection () {
    const options = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }
    await mongoose.connect(`${apiSettings.DATABASE_URI}/${apiSettings.DATABASE_NAME}`, options)
  }

  setupBodyParser () {
    this.app.use(bodyParser.json())
  }

  setupRouter () {
    this.app.use(apiSettings.BASE_PATH, routes)
  }
}

module.exports = App
