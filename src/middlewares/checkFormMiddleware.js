const checkFormMiddleware = function (req, res, next) {
  const { user, curriculum } = req.body

  if (!user || !curriculum) {
    return res
      .status(400)
      .json({
        success: false,
        message: 'User register not created, required forms missing!',
        forms: {
          expected: ['user', 'curriculum'],
          received: Object.keys(req.body),
        },
      })
  }

  next()
}

module.exports = checkFormMiddleware
