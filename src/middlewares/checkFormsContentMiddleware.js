const checkFormsContentMiddleware = function (req, res, next) {
  const { user, curriculum } = req.body
  const { name, address, username } = user
  const { type } = curriculum

  if (!name || !name.firstName || !name.lastName || !address || !username || !type) {
    let receivedFields = [...Object.keys(user)]
    if (type) receivedFields = [...receivedFields, 'type']

    return res
      .status(400)
      .json({
        success: false,
        message: 'User register not created, missing required fields!',
        fields: {
          expected: ['name', 'address', 'username', 'type'],
          received: receivedFields,
        },
      })
  }

  next()
}

module.exports = checkFormsContentMiddleware
