const UserModel = require('../models/userModel')

const checkUserRegisterMiddleware = async function (req, res, next) {
  const { username } = req.body.user

  const user = await UserModel.findOne({ username }).lean()

  if (user) {
    return res
      .status(400)
      .json({
        success: false,
        message: `User ${username} already exist`,
      })
  }

  next()
}

module.exports = checkUserRegisterMiddleware
