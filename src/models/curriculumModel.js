const mongoose = require('mongoose')

const curriculumSchema = mongoose.Schema({
  username: String,
  type: String,
  experiences: [{
    title: String,
    description: String,
  }],
  qualifications: [{
    qualification: String,
  }],
  languages: [{
    language: String,
  }],
})

module.exports = mongoose.model('Curriculum', curriculumSchema)
