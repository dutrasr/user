const request = require('supertest')
const chai = require('chai')

const App = require('../../src/app')
const pkg = require('../../package.json')

describe('User API (health)', () => {
  const expect = chai.expect
  let app = null

  before(async () => {
    app = await new App().setup()
    await app.listen()
  })

  describe('/health', () => {
    it('should return the api health info', async () => {
      const response = await request(app)
        .get('/health')

      expect(response).to.have
        .property('status', 200)

      expect(response.body).to.have
        .property('app', pkg.name)

      expect(response.body).to.have
        .property('version', pkg.version)
    })
  })
})
