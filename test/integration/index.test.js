const request = require('supertest')
const chai = require('chai')

const App = require('../../src/app')
const pkg = require('../../package.json')

describe('User API (Index)', () => {
  const expect = chai.expect
  let app = null

  before(async () => {
    app = await new App().setup()
    await app.listen()
  })

  describe('/', () => {
    it('should return the api description', async () => {
      const resp = await request(app)
        .get('/')

      expect(resp.body).to.have
        .property('message', pkg.description)
    })
  })
})
