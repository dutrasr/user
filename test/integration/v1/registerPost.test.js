const request = require('supertest')
const chai = require('chai')

const App = require('../../../src/app')
const UserModel = require('../../../src/models/userModel')
const CurriculumModel = require('../../../src/models/curriculumModel')

describe('User API (Post Register)', () => {
  const expect = chai.expect
  let app = null

  const name = { firstName: 'Eric', lastName: 'Idle' }
  const address = '424 Humphreys St, Nashville'
  const username = 'idle.eric'
  const type = 'Developer'
  const experiences = [
    { title: 'nodejs', description: 'write nice backend apps' },
    { title: 'react', description: 'know react lifecycle' },
    { title: 'vuejs', description: 'learning vuejs' },
  ]
  const qualifications = [
    { qualification: 'mocha' },
    { qualification: 'node' },
    { qualification: 'express' },
  ]

  const languages = [{ language: 'English' }]

  before(async () => {
    app = await new App().setup()
    await app.listen()
  })

  beforeEach(async () => {
    await UserModel.deleteMany({})
    await CurriculumModel.deleteMany({})
  })

  after(async () => {
    await UserModel.collection.drop()
    await CurriculumModel.collection.drop()
  })

  describe('POST /v1/user', () => {
    describe('when form is valid', () => {
      describe('when user do not exist', () => {
        it('should create the user register', async () => {
          const resp = await request(app)
            .post('/v1/user/register')
            .set('Content-Type', 'application/json')
            .send({
              user: {
                name,
                address,
                username,
              },
              curriculum: {
                username,
                type,
                experiences,
                qualifications,
                languages,
              },
            })

          expect(resp.status).to
            .eq(200)

          expect(resp.body).to.have
            .property('success', true)

          expect(resp.body).to.have
            .property('message', 'User register with success!')
        })
      })

      describe('when user already exist', () => {
        it('should return user already exist', async () => {
          const User = new UserModel({ name, address, username })
          await User.save()

          const resp = await request(app)
            .post('/v1/user/register')
            .set('Content-Type', 'application/json')
            .send({
              user: {
                name,
                address,
                username,
              },
              curriculum: {
                username,
                type,
                experiences,
                qualifications,
                languages,
              },
            })

          expect(resp.status).to
            .eq(400)

          expect(resp.body).to.have
            .property('success', false)

          expect(resp.body).to.have
            .property('message', `User ${username} already exist`)
        })
      })
    })

    describe('when the form is not valid', () => {
      describe('when a form required field is missing', () => {
        it('should return the expected and the received fields (user - username, curriculum - type)', async () => {
          const resp = await request(app)
            .post('/v1/user/register')
            .set('Content-Type', 'application/json')
            .send({
              user: {
                name,
                address,
              },
              curriculum: {
                username,
                experiences,
                qualifications,
                languages,
              },
            })

          expect(resp.status).to
            .eq(400)

          expect(resp.body).to.have
            .property('success', false)

          expect(resp.body).to.have
            .property('message', 'User register not created, missing required fields!')

          expect(resp.body.fields.expected).to
            .members(['name', 'address', 'username', 'type'])

          expect(resp.body.fields.received).to
            .members(['name', 'address'])
        })
      })

      describe('when a form is missing', () => {
        it('should return the missing form (user)', async () => {
          const resp = await request(app)
            .post('/v1/user/register')
            .set('Content-Type', 'application/json')
            .send({
              curriculum: {
                username,
                type,
                experiences,
                qualifications,
                languages,
              },
            })

          expect(resp.status).to
            .eq(400)

          expect(resp.body).to.have
            .property('success', false)

          expect(resp.body).to.have
            .property('message', 'User register not created, required forms missing!')

          expect(resp.body.forms.expected).to
            .members(['user', 'curriculum'])

          expect(resp.body.forms.received).to
            .members(['curriculum'])
        })

        it('should return the missing form (curriculum)', async () => {
          const resp = await request(app)
            .post('/v1/user/register')
            .set('Content-Type', 'application/json')
            .send({
              user: {
                name,
                address,
                username,
              },
            })

          expect(resp.status).to
            .eq(400)

          expect(resp.body).to.have
            .property('success', false)

          expect(resp.body).to.have
            .property('message', 'User register not created, required forms missing!')

          expect(resp.body.forms.expected).to
            .members(['user', 'curriculum'])

          expect(resp.body.forms.received).to
            .members(['user'])
        })
      })
    })
  })
})
